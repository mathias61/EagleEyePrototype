package com.example.eagleeye.routegenerator;

import com.google.android.gms.maps.model.Marker;

public class CustomLatLng {
    private double lat;
    private double lng;
    private Marker marker;

    public CustomLatLng(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
    public CustomLatLng(double lat, double lng, Marker marker) {
        this.lat = lat;
        this.lng = lng;
        this.marker = marker;
    }

    public double getLat() {return lat;}
    public double getLng() {return lng;}
    public void setMarker(Marker marker) {this.marker = marker;}
    public Marker getMarker() {return marker;}
    public String toString() {return lat + "," + lng;}
}
