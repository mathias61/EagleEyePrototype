package com.example.eagleeye;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eagleeye.databinding.TextLayoutBinding;
import com.example.eagleeye.menu.MenuItem;
import com.example.eagleeye.menu.MenuItemAdapter;
import com.example.eagleeye.routegenerator.CustomLatLng;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.example.eagleeye.databinding.ActivityMapsBinding;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final String CHANNEL_ID = "10001";
    public static final String TAG = "MainActivity";
    public static int id = 0;
    public static final String[] DESTINATIONS = new String[]{

    };
    public static MenuItemAdapter mAdapter;
    public static List<MenuItem> mModels;
    public static RecyclerView mRecyclerView;
    public static CustomLatLng currentPos;
    public static CustomLatLng newPos = null;
    public static ArrayList path = new ArrayList();
    public static ArrayList<PolyLineExtended> currentRoutes = new ArrayList<>();
    public static ArrayList<CustomLatLng> crimes = new ArrayList<>();
    public static ArrayList<LatLng> destinations = new ArrayList<>();
    public static ArrayList<Marker> destinationGrocery = new ArrayList<>();
    public static boolean routeExists = false;
    public static boolean addCrime = false;
    public static Marker mark;

    public static final Comparator<MenuItem> ALPHABETICAL_COMPARATOR = new Comparator<MenuItem>() {
        @Override
        public int compare(MenuItem a, MenuItem b) {
            return a.getAddress().compareTo(b.getAddress());
        }
    };

    public static GoogleMap mMap = null;
    public static ActivityMapsBinding binding;
    public static TextLayoutBinding mBinding;
    public static SearchView searchView;
    public static int zoomLevel = 14;
    public static Button routingBtn;
    public static Switch switchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentPos = new CustomLatLng(40.114399, -88.223961); //Siebel center
        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);

        //setSupportActionBar(myToolbar);
        searchView = findViewById(R.id.idSearchView);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        searchBarSetup();
        //testBarSetup();
        mapFragment.getMapAsync(this);

        LatLng county_market = new LatLng(40.11374611502624, -88.23405236012275);
        LatLng amko = new LatLng(40.11254816601727, -88.2385370135618);
        LatLng target = new LatLng(40.11049683446659, -88.23003977546674);
        destinations.add(county_market);
        destinations.add(amko);
        destinations.add(target);

        routingBtn = findViewById(R.id.routebtn);

        routingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPos == null) {
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_LONG;
                    Toast errorMessage = Toast.makeText(context, "Error: Please enter a destination first!", duration);

                    errorMessage.show();
                }else {
                    path = new ArrayList();
                    if (!routeExists) {
                        routeGenerator(currentPos, newPos, true);
                        routeExists = true;
                        routingBtn.setText("Find New Route");
                    } else {
                        reRoute();
                    }
                }
            }
        });

        createNotificationChannel();
    }


    @Override
    protected void onPause() {
        addCrime = true;
        sendNotification();
        super.onPause();
    }


    private void testBarSetup() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.text_layout);
        mAdapter = new MenuItemAdapter(this, ALPHABETICAL_COMPARATOR);
        //mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //mBinding.recyclerView.setAdapter(mAdapter);

        mModels = new ArrayList<>();
        for (String destination : DESTINATIONS) {
            mModels.add(new MenuItem(destination, "test"));
        }
        mAdapter.add(mModels);
    }

    private void searchBarSetup() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();

                // TEMP DESTINATION SELECTION
                if (location.equals("grocery store")) {
                    destinationGrocery.add(mMap.addMarker(new MarkerOptions().position(destinations.get(0)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)).title("County Market").snippet("331 E Stoughton St")));
                    destinationGrocery.add(mMap.addMarker(new MarkerOptions().position(destinations.get(1)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)).title("Amko").snippet("101 E Springfield Ave")));
                    destinationGrocery.add(mMap.addMarker(new MarkerOptions().position(destinations.get(2)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)).title("Target").snippet("603 E Green St")));

                    LatLng cntr = new LatLng(40.111957, -88.232743);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cntr, zoomLevel));
                }

                List<Address> addressList = null;

                if (location != null || location.equals("")) {
                    // on below line we are creating and initializing a geo coder.
                    Geocoder geocoder = new Geocoder(MapsActivity.this);
                    try {
                        // on below line we are getting location from the
                        // location name and adding that location to address list.
                        addressList = geocoder.getFromLocationName(location, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // on below line we are getting the location
                    // from our list a first position.
                    if (addressList.size() == 0) {
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_LONG;
                        Toast errorMessage = Toast.makeText(context, R.string.location_not_found_try_again, duration);
                        View toastView = errorMessage.getView();
                        //toastView.setBackgroundResource(R.color.red);
                        errorMessage.show();
                        return false;
                    }
                    Address address = addressList.get(0);

                    // on below line we are creating a variable for our location
                    // where we will add our locations latitude and longitude.
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

                    // on below line we are adding marker to that position.
                    Marker newPosMarker = mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
                    if (newPos == null) {
                        newPos = new CustomLatLng(address.getLatitude(), address.getLongitude(), newPosMarker);
                    } else {
                        newPos.getMarker().remove();
                        if (currentRoutes.size() > 0) {
                            for (PolyLineExtended route: currentRoutes
                                 ) {
                                route.getRoute().remove();
                            }
                        }
                        newPos = new CustomLatLng(address.getLatitude(), address.getLongitude(), newPosMarker);
                    }
                    // below line is to animate camera to that position.
                    //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                }


                routeExists = false;
                routingBtn.setText("Generate Route");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notification_name);
            String description = getString(R.string.notification_content);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void sendNotification() {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.eagle_eye_logo)
                .setContentTitle("EagleEye")
                .setContentText("A crime has appeared, click to reroute")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        int notificationId = 1;
        notificationManager.notify(notificationId, builder.build());
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        System.out.println("Is this called again?");

        LatLng siebel = new LatLng(currentPos.getLat(), currentPos.getLng());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(siebel, zoomLevel));

        mMap.addMarker(new MarkerOptions().position(siebel).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.current_pos))));

        LatLng yellow_zone1 = new LatLng(40.110897, -88.220381);
        LatLng yellow_zone2 = new LatLng(40.116676, -88.270752);
//        mMap.addMarker(new MarkerOptions().position(yellow_zone1).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.yellow_danger_zone))));
//        mMap.addMarker(new MarkerOptions().position(yellow_zone2).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.yellow_danger_zone))));
        mMap.addCircle(new CircleOptions()
                .center(yellow_zone1)
                .radius(80)
                .strokeColor(0xf0ec1a)
                .fillColor(0x80f0ec1a));

        mMap.addCircle(new CircleOptions()
                .center(yellow_zone2)
                .radius(80)
                .strokeColor(0xf0ec1a)
                .fillColor(0x80f0ec1a));

        LatLng red_zone = new LatLng(40.116223, -88.229258);
        //mMap.addMarker(new MarkerOptions().position(red_zone).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.red_danger_zone))));
//        mMap.addPolygon(new PolygonOptions().addAll(sCountryBorder).fillColor(0xcc000000));
        mMap.addCircle(new CircleOptions()
                .center(red_zone)
                .radius(50)
                .strokeColor(Color.RED)
                .fillColor(0x73FF0000));

        LatLng orange_zone = new LatLng(40.106351, -88.233444);
      //  mMap.addMarker(new MarkerOptions().position(orange_zone).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.orange_danger_zone))));
        mMap.addCircle(new CircleOptions()
                .center(orange_zone)
                .radius(75)
                .strokeColor(0xf77728)
                .fillColor(0x73f77728));

        // 40.106977, -88.232779
        LatLng assault_loc = new LatLng(40.106977,  -88.232779);
        mMap.addMarker(new MarkerOptions().position(assault_loc).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.assault_icon))).title("Aggravated Assault").snippet("12/1/22 - 2:31 AM"));

        //40.116374, -88.229105
        LatLng shooting_loc = new LatLng(40.116374,  -88.229105);
        mMap.addMarker(new MarkerOptions().position(shooting_loc).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.shooting_icon))).title("Shooting").snippet("10/24/22 - 10:58 PM"));

        //40.116215, -88.268703, 40.115874, -88.229853
        LatLng theft_loc1 = new LatLng(40.116215,  -88.268703);
        mMap.addMarker(new MarkerOptions().position(theft_loc1).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.robbery_icon))).title("Robbery").snippet("11/14/22 - 4:28 PM"));

        LatLng theft_loc2 = new LatLng(40.115874, -88.229853);
        mMap.addMarker(new MarkerOptions().position(theft_loc2).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.robbery_icon))).title("Robbery").snippet("11/21/11 - 12:04 AM"));

        // 40.115313, -88.235491
        LatLng arrest_loc = new LatLng(40.112465, -88.235301);
        mMap.addMarker(new MarkerOptions().position(arrest_loc).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.arrest_icon))).title("Arrest").snippet("11/30/22 - 12:42 AM"));

        if (newPos != null) {
            System.out.println("Adding patH: " + path.size());
            updateRoute();
            LatLng des = new LatLng(newPos.getLat(), newPos.getLng());
            System.out.println(des);
            mMap.addMarker(new MarkerOptions().position(des));
        }

        if (addCrime) {
            routingBtn.setText("Find New Route");
            addCrime();
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                for (Marker des: destinationGrocery
                     ) {
                    System.out.println("Checking marker: " + marker.getTitle());
                    if (Objects.equals(marker.getTitle(), des.getTitle())) {
                        routeGenerator(currentPos, (new CustomLatLng(des.getPosition().latitude, des.getPosition().longitude)), true);
                        break;
                    }
                }


                if (marker.isInfoWindowShown()) {
                    marker.hideInfoWindow();
                } else {
                    marker.showInfoWindow();
                }
                return false;
            }
        });

    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public void addCrimeHardcode() {
        CustomLatLng latLng = generateRandomPoint();
        // siebel: 40.114399, -88.223961
        // 40.102610, -88.233138
        // 40.110715, -88.223778
        //40.112548, -88.264886
        // LatLng temp = new LatLng(latLng.getLat(), latLng.getLng());
        LatLng temp = new LatLng(40.112548, -88.264886);
        Marker marker = mMap.addMarker(new MarkerOptions().position(temp).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.crime_notification))).title("Recent Crime Alert").snippet("Robbery reported 10 minutes ago. Police have not located the suspect."));
        latLng.setMarker(marker);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                temp, zoomLevel));
        crimes.add(latLng);
    }

    public Marker addCrime() {
        int i = 0;
        while(i < 1) {
            CustomLatLng latLng = generateRandomPoint();
            LatLng temp = new LatLng(latLng.getLat(), latLng.getLng());
            Marker marker = mMap.addMarker(new MarkerOptions().position(temp).icon(BitmapDescriptorFactory.fromBitmap(getBitmap(R.drawable.crime_notification))).title("Recent Crime Alert").snippet("Robbery reported just now. Police are on their way."));
            marker.showInfoWindow();
            latLng.setMarker(marker);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    temp, zoomLevel));
            crimes.add(latLng);
            mark = marker;
            i++;
        }

        return mark;
    }
    public void remove(Marker marker) {
        marker.setVisible(false);
    }

    public void reRoute() {
        CustomLatLng latLng = generateRandomPoint();

        // on below line we are adding marker to that position.
        for (PolyLineExtended route: currentRoutes
        ) {
            System.out.println("Now removing these routes: " + id);
            route.getRoute().remove();
        }
        currentRoutes.clear();

        path = new ArrayList();
        routeGenerator(currentPos, latLng, true);
        path = new ArrayList();
        routeGenerator(latLng, newPos, false);
    }

    public CustomLatLng generateRandomPoint() {
        double xMin = Math.min(currentPos.getLng(), newPos.getLng());
        double xMax = Math.max(currentPos.getLng(), newPos.getLng());

        double yMin = Math.min(currentPos.getLat(), newPos.getLat());
        double yMax = Math.max(currentPos.getLat(), newPos.getLat());

        double lat = yMin + Math.random() * (yMax - yMin); //y
        double lng = xMin + Math.random() * (xMax - xMin); //x

       return new CustomLatLng(lat, lng, null);
    }

    public void routeGenerator(CustomLatLng pos, CustomLatLng dest, boolean shouldClear) {
        //newPos = new CustomLatLng(40.115399, -88.224961);
        //path = new ArrayList();
        //Execute Directions API request
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyAQz48_tVPVqxvkilKEmDk_rdTa0oZ6gXQ")
                .build();
        DirectionsApiRequest req = DirectionsApi.getDirections(context, pos.toString(), dest.toString());

        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs != null) {
                    for (int i = 0; i < route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j = 0; j < leg.steps.length; j++) {
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length > 0) {
                                    for (int k = 0; k < step.steps.length; k++) {
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getLocalizedMessage());
        }

        //Draw the polyline
        if (path.size() > 0) {
            updateRoute(shouldClear);
        }

        mMap.getUiSettings().setZoomControlsEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        //final MenuItem searchItem = menu.findItem(R.id.action_search);
        //final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                final List<MenuItem> filteredModelList = filter(mModels, query);
                mAdapter.replaceAll(filteredModelList);
                //mBinding.recyclerView.scrollToPosition(0);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });
        return true;
    }

    private static List<MenuItem> filter(List<MenuItem> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<MenuItem> filteredModelList = new ArrayList<>();
        for (MenuItem model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private void updateRoute() {
        updateRoute(true);
    }

    private void updateRoute(boolean shouldClear) {
        if (shouldClear && currentRoutes.size() > 0) {
            for (PolyLineExtended route: currentRoutes
                 ) {
                System.out.println("Now removing these routes: " + id);
                route.getRoute().remove();
            }
            currentRoutes.clear();
            System.out.println("List should be empty: size: " + currentRoutes.size());
            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(7);
            Polyline route = mMap.addPolyline(opts);
            currentRoutes.add(new PolyLineExtended(route, opts, id++));
        } else {
            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(7);
            Polyline route = mMap.addPolyline(opts);
            currentRoutes.add(new PolyLineExtended(route, opts, id++));
        }


    }


    private class PolyLineExtended {
        private Polyline route;
        private PolylineOptions routeOptions;
        private int id;

        public PolyLineExtended(Polyline route, PolylineOptions routeOptions, int id) {
            this.route = route;
            this.routeOptions = routeOptions;
            this.id = id;
        }

        public Polyline getRoute() {return route;}
        public PolylineOptions getRouteOptions() {return routeOptions;}
        public int getId() {return id;}

    }

}