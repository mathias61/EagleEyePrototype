package com.example.eagleeye.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.example.eagleeye.databinding.TextLayoutBinding;

import java.util.Comparator;
import java.util.List;

public class MenuItemAdapter extends RecyclerView.Adapter<MenuViewHolder> {
    private final SortedList<MenuItem> mSortedList = new SortedList<MenuItem>(MenuItem.class, new SortedList.Callback<MenuItem>() {
        @Override
        public int compare(MenuItem a, MenuItem b) {
            return mComparator.compare(a, b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(MenuItem oldItem, MenuItem newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(MenuItem item1, MenuItem item2) {
            return item1.getAddress() == item2.getAddress();
        }
    });

    private final LayoutInflater mInflater;
    private final Comparator<MenuItem> mComparator;

    public MenuItemAdapter(Context context, Comparator<MenuItem> comparator) {
        mInflater = LayoutInflater.from(context);
        mComparator = comparator;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final TextLayoutBinding binding = TextLayoutBinding.inflate(mInflater, parent, false);
        return new MenuViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        final MenuItem model = mSortedList.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return mSortedList.size();
    }

    public void add(MenuItem model) {
        mSortedList.add(model);
    }

    public void remove(MenuItem model) {
        mSortedList.remove(model);
    }

    public void add(List<MenuItem> models) {
        mSortedList.addAll(models);
    }

    public void remove(List<MenuItem> models) {
        mSortedList.beginBatchedUpdates();
        for (MenuItem model : models) {
            mSortedList.remove(model);
        }
        mSortedList.endBatchedUpdates();
    }

    public void replaceAll(List<MenuItem> models) {
        mSortedList.beginBatchedUpdates();
        for (int i = mSortedList.size() - 1; i >= 0; i--) {
            final MenuItem model = mSortedList.get(i);
            if (!models.contains(model)) {
                mSortedList.remove(model);
            }
        }
        mSortedList.addAll(models);
        mSortedList.endBatchedUpdates();
    }

}
