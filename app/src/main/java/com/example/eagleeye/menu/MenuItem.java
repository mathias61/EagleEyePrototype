package com.example.eagleeye.menu;

import java.util.Objects;

public class MenuItem {
    private final String name;
    private final String address;

    public MenuItem(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuItem model = (MenuItem) o;

        if (!Objects.equals(address, model.address)) return false;
        return name != null ? name.equals(model.name) : model.name == null;

    }
}
