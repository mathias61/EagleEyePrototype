package com.example.eagleeye.menu;

import androidx.recyclerview.widget.RecyclerView;

import com.example.eagleeye.databinding.TextLayoutBinding;
import com.example.eagleeye.menu.MenuItem;

public class MenuViewHolder  extends RecyclerView.ViewHolder {
    private final TextLayoutBinding mBinding;

    public MenuViewHolder(TextLayoutBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bind(MenuItem item) {
        mBinding.setMenuItem(item);
    }
}
